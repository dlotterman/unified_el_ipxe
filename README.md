# Unified_Enterprise Linux 9 install-chain for iPXE

This repository is a collection of resources that demonstrate a path to end to end automated Enterprise Linux (9) deployments with modest best practices application in iPXE enabled environments. It is far from comprehensive and not meant to be perscriptive, the beuaty of this ecosystem is it's flexibility in providing multiple valid paths to the same end, this just aims to show one of them.

Everything in this repository should be considered unsupported by the author, self-supported by the consumer / operator.

The current state of this README is just an initial dump, it will be packaged better over time. This is before the creation of TODO.md so to speak.

Current state of this repo should be considered failing :red_circle:, files mangling happening in public.

Checkboxes marked by this toolchain:

- "Automateable and repeatable bootstrap" via iPXE
- No touch automated install
- UEFI vs BIOS awareness from boot to kernel upgrade
- RAID (software via mdadm, includes BIOS vs UEFI boot awareness for 2x disk RAID-1 local boot)
- Automatic hardware detection, broad hardware support, additional hardware supported via DUD/DUP disk
    - Installs to a VM as well as a BM
- Can support OpenSCAP and [security centric](https://ipxe.org/crypto) installs with a little thought
- Provides an opinionated path to merge `cloud-init` like functionality and complicated tunrup in a post-first-boot environment
    - Essentially a oneshot (well, two shot) configuration stage that triggers a final reboot into the production-service ready instance
    - This path enables the cleanest consistent path for solving really hard things like bugs in NIC drivers and complicated network or storage discovery before entering service.
    - `cloud-init` can still be enabled, it's as simple as installing it as a package in %packages
