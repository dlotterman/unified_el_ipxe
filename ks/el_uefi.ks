#version=RHEL9
text

%addon com_redhat_kdump --disable

%end

%addon com_redhat_oscap
    content-type = scap-security-guide
    datastream-id = scap_org.open-scap_datastream_from_xccdf_ssg-rhel9-xccdf-1.2.xml
    xccdf-id = scap_org.open-scap_cref_ssg-rhel9-xccdf-1.2.xml
    profile = xccdf_org.ssgproject.content_profile_ospp
%end

keyboard --xlayouts='us'

lang en_US.UTF-8

# https://github.com/Ch00k/minuteware.net/blob/master/source/_posts/2012-10-18-random-hostname-for-centos-kickstart-installation.md
%pre
echo "network --hostname=`echo centos-$RANDOM$RANDOM`" > /tmp/pre-hostname
%end

services --disabled=sshd,iscsid

%packages
@^minimal-environment
@guest-agents
@headless-management
audit
chrony
cloud-init
crypto-policies
dnf-automatic
fapolicyd
firewalld
gnutls-utils
jq
openscap
openscap-scanner
openssh-clients
openssh-server
scap-security-guide
subscription-manager
sudo
tmux
usbguard

%end

firstboot --enable

skipx
clearpart --all --initlabel

# ======================================== Storage %Pre ==============================================
# Storage. Preference for lsblk for comminality with authors tooling.
# readability is prefered over bashfooability
%pre --log=/root/ks_pre_part_include.log

lsblk --bytes | grep -v -E "nvme|zram" | grep disk | awk '{print$4,$1}' | sort -n  | head -2 | awk '{print$2}' | tr '\n'
',' | awk '{print"ignoredisk --only-use="$1}'| sed 's/,$/\n/' > /tmp/part-include

DRIVES=$(lsblk --bytes | grep -v -E "nvme|zram" | grep disk | awk '{print$4,$1}' | sort -n  | head -2 | awk '{print$2}')

echo "bootloader --location=mbr
--password="grub.pbkdf2.sha512.10000.
$LONGHASH" --boot-drive=$(echo $DRIVES | tr ' ' '\n' | head -1) --driveorder=$(echo $DRIVES | tr ' ' ',')" >> /tmp/part-include

# raid index 1 is reserved for efi, see efi ks
for DRIVE in $DRIVES
do
        echo "part raid.1$DRIVE --fstype="mdmember" --ondisk=$DRIVE --size=576 --asprimary" >> /tmp/part-include
        echo "part raid.2$DRIVE --fstype="mdmember" --ondisk=$DRIVE --size=1024 --asprimary" >> /tmp/part-include
        echo "part raid.3$DRIVE --fstype="mdmember" --ondisk=$DRIVE --size=2048" >> /tmp/part-include
        echo "part raid.4$DRIVE --fstype="mdmember" --ondisk=$DRIVE --size=8000" >> /tmp/part-include
        echo "part raid.5$DRIVE --fstype="mdmember" --ondisk=$DRIVE --size=4000 --grow" >> /tmp/part-include
done

echo $DRIVES | awk '{print"raid /boot/efi --device=boot_efi --fstype=\"efi\" --level=RAID1 --fsoptions=\"umask=0077,shortname=winnt\" --label=boot_efi raid.1"$1" raid.1"$2}' >> /tmp/part-include

echo $DRIVES | awk '{print"raid swap --device=swap --fstype=\"swap\" --level=RAID1 raid.3"$1" raid.3"$2}' >>
/tmp/part-include

echo $DRIVES | awk '{print"raid /boot --device=boot --fstype=\"ext3\" --level=RAID1 --label=boot raid.2"$1" raid.2"$2}'
>> /tmp/part-include

echo $DRIVES | awk '{print"raid / --device=root --fstype=\"xfs\" --level=RAID1 raid.4"$1" raid.4"$2}' >>
/tmp/part-include

echo $DRIVES | awk '{print"raid pv.01 --device=pv_01 --fstype="lvmpv" --level=RAID1 raid.5"$1" raid.5"$2}' >>
/tmp/part-include

echo "volgroup vg_01 --pesize=4096 pv.01" >> /tmp/part-include
echo "logvol /var --fstype="xfs" --percent=15 --label="lv_var" --name=lv_var --vgname=vg_01 >> /tmp/part-include
echo "logvol /home --fstype="xfs" --percent=5 --label="lv_home" --name=lv_home --vgname=vg_01 >> /tmp/part-include
echo "logvol /srv --fstype="xfs" --size=32 --label="lv_srv" --name=lv_srv --vgname=vg_01 >> /tmp/part-include
echo "logvol /var/log/audit --fstype="xfs" --percent=2 --label="lv_var_audit" --name=lv_var_audit --vgname=vg_01 >> /tmp/part-include
echo "logvol /var/tmp --fstype="xfs" --size=1024 --label="lv_var_tmp" --name=lv_var_tmp --vgname=vg_01 --grow >> /tmp/part-include
echo "logvol /tmp --fstype="xfs" --size=1024 --label="lv_tmp" --name=lv_tmp --vgname=vg_01 >> /tmp/part-include
echo "logvol /opt --fstype="xfs" --size=32 --label="lv_opt" --name=lv_opt --vgname=vg_01 >> /tmp/part-include
echo "logvol /var/log --fstype="xfs" --percent=5 --label="lv_var_log" --name=lv_var_log --vgname=vg_01 >> /tmp/part-include

%end
# ======================================= End Storage %Pre ==================================================
%include /tmp/part-include

# ======================================= unified-el-init %Post ===========================
%post --log=/root/ks_unified_el_init.log

mkdir -p /var/tmp/unified_el_init/unified_el_init
cat > /var/tmp/unified_el_init/unified_el_init/unified_el_init.sh << EOL
if test -f /var/tmp/unified_el_init/unified_el_init_1.lock; then
    logger -s "unified_el_init: upgrade from install -> current should be done"
        if test -f /var/tmp/unified_el_init/unified_el_init_2.lock; then
            logger -s "unified_el_init: init should be complete, exiting"
            exit 0
        else
            logger -s "unified_el_init: finizaling initialization"
            systemctl enable --now sshd
            systemctl disable unified_el_init.service
            touch /var/tmp/unified_el_init/unified_el_init_2.lock
            chmod -R 0400 /var/tmp/unified_el_init/
        fi

else
    logger -s "unified_el_init: applying configuration, upgrading and rebooting instance"
    systemctl enable --now firewalld
    firewall-cmd --permanent --zone=public --set-target=DROP
    firewall-cmd --permanent --zone=public --add-service=ssh
    firewall-cmd --reload
    sudo -u adminuser01 mkdir -p /home/adminuser01/.ssh/
    sudo -u adminuser01 touch /home/adminuser01/.ssh/authorized_keys
    sudo -u adminuser01 curl -s https://github.com/your-username.keys >> /home/adminuser01/.ssh/authorized_keys
    echo "AllowAgentForwarding no" >> /etc/ssh/sshd_config
    echo "PermitRootLogin no" >> /etc/ssh/sshd_config
    sed -i -e '/^#MaxAuthTries/s/^.*$/MaxAuthTries 5/' /etc/ssh/sshd_confi
    sed -i -e '/^X11Forwarding/s/^.*$/X11Forwarding no/' /etc/ssh/sshd_config
    echo "fastestmirror=True" >> /etc/dnf/dnf.conf
    echo "max_parallel_downloads=10" >> /etc/dnf/dnf.conf
    systemctl enable --now dnf-automatic.timer
    sleep 1
    dnf upgrade -y --refresh
    sync
    sleep 1
    logger "unified_el_init: upgrade complete, rebooting"
    touch /var/tmp/unified_el_init/unified_el_init_1.lock
    reboot
    exit 0

fi
EOL

cat > /etc/systemd/system/unified_el_init.service << EOL
[Unit]
Description="unified_el_init"
Requires=network-online.target

[Service]
Type=oneshot
TimeoutStartSec=900s
ExecStart=bash /var/tmp/unified_el_init/unified_el_init.sh

[Install]
WantedBy=multi-user.target
EOL

timesource --ntp-pool=2.rocky.pool.ntp.org
timesource --ntp-pool=3.pool.ntp.org

timezone Etc/GMT --utc

# https://ma.ttias.be/how-to-generate-a-passwd-password-hash-via-the-command-line-on-linux/
rootpw --iscrypted $YOURHASH
user --groups=wheel --name=adminuser01 --password=$YOURHASH --iscrypted --gecos="adminuser01"

reboot